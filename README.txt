Raspberry Pi Kiwi Drive

Noah Tatko
March 3, 2018

1: What is Kiwi Drive?

The answer is simple: Kiwi drive is a form of holonomic drivetrain, which uses
only three wheels. The wheels provide power in the shape of an equilateral
triangle, using slip and vector physics to precisely control the direction of
the robot in question. Sadly, there is loss of power: at any given time, a
kiwi drive robot can only utilize from 58% to 66% of the motor's power at any
given time. The remaining power is lost to slip due to the vectors.

But I'll assume you know what you want to know about kiwi drive systems, as you
are already looking into making your own.


2: What do I need?

In truth, not much. You will need a Raspberry Pi Zero W (OR) a Raspberry Pi 3,
2x Adafruit TB6612 1.2A DC/Stepper Motor Driver Breakout Board, and 3 of the
same DC motors, pretty much whatever you want. You'll need to calculate your own
speed, torque, and direction. My own application is
