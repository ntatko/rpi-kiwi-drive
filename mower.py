# mower.py

#------------------------------------
#
# Noah Tatko
# tatko.noah@gmail.com
#
#------------------------------------

import time
import RPi.GPIO as GPIO
import requests
import math

#### Declare Node.js variables ####

baseURL = "http://localhost:8080"
controllerURL = "/values".format(baseURL, 5)

# payload = {'x1': 2, 'y1': 1, 'x2': 50, 'y2': 35}
# response = requests.post(baseURL + controllerURL, data=payload)
#
# print(response.text)  # TEXT/HTML

#### Initialize motors ####

# Declare the GPIO settings
GPIO.setmode(GPIO.BOARD)

# set up GPIO pins -- RIGHT NOW ONLY DRIVING TWO MOTORS WITH ONE BOARD. NEED TO SETUP
# THE NEXT BOARD TO WORK AS A THREESOME
GPIO.setup(7, GPIO.OUT) # Connected to PWMA
GPIO.setup(11, GPIO.OUT) # Connected to AIN2
GPIO.setup(12, GPIO.OUT) # Connected to AIN1
GPIO.setup(13, GPIO.OUT) # Connected to STBY
GPIO.setup(15, GPIO.OUT) # Connected to BIN1
GPIO.setup(16, GPIO.OUT) # Connected to BIN2
GPIO.setup(18, GPIO.OUT) # Connected to PWMB

# these ports may need to be reassigned
GPIO.setup(19, GPIO.OUT) # Connected to PWMA
GPIO.setup(20, GPIO.OUT) # Connected to AIN2
GPIO.setup(21, GPIO.OUT) # Connected to AIN1
GPIO.setup(22, GPIO.OUT) # Connected to STBY

#### While Loop ####

mower = FALSE

stopper = FALSE
while not stopper:

#### Get values from node server #### NEED TO ADD BUTTON SUPPORT AND KILL SWITCH

    response = requests.get(baseURL + controllerURL)

#debug
    print response.text

    inputs = response.text.encode('ascii', 'ignore').split("\"")
    inputs.remove("{")
    inputs.remove(":")
    inputs.remove(",")

#debug
    print inputs
    # wrong inputs. Recode.
    received = [[inputs[0], inputs[1]], [inputs[2], inputs[3][1]], [inputs[4], inputs[5][1]],
                [inputs[6], inputs[7][1]]]
#debug
    print received

    #### Change above into simpler terms, for physics sake ####

    x1 = recieved[0][0]
    y1 = received[0][1]
    x2 = recieved[1][0]
    y2 = recieved[1][1]

    rotation_constant = 0.25 #arbitrary, see if this number is too high or low in testing

    #### Now for the vector physics ####

    # it is yet unconfirmed if this math is accurate. I just sort of came up with
    # it while forgetting everything I ever knew about trig, so be sure to test
    # drive thoroughly.
    motorAPower = (y1 * math.sqrt(3) / 2 + x1 / 2 + rotation_constant * x2) * 100
    motorBPower = (-y1 / 2 - x1 * math.sqrt(3) / 2 + rotation_constant * x2) * 100
    motorCPower = (-x1 - rotation_constant * x2) * 100

    #### Drive the motors ####

    if motorAPower > 0:
        #GPIO.setup(7, GPIO.OUT) # Connected to PWMA
        #GPIO.setup(11, GPIO.OUT) # Connected to AIN2
        #GPIO.setup(12, GPIO.OUT) # Connected to AIN1
        GPIO.output(7, GPIO.HIGH) # Set PWMA
        
