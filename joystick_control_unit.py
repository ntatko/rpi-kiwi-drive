# joystick_control_unit.py

#------------------------------------
#
# Noah Tatko
# tatko.noah@gmail.com
#
#------------------------------------

# NEED TO ADD BUTTON SUPPORT

import pygame
import requests

baseURL = "http://localhost:8080"
controllerURL = "/values".format(baseURL, 5)

pygame.init()
print "Joystics: ", pygame.joystick.get_count()
my_joystick = pygame.joystick.Joystick(0)
my_joystick.init()
clock = pygame.time.Clock()

clock.tick(200)

joystickoffsets = [my_joystick.get_axis(0), my_joystick.get_axis(1), -my_joystick.get_axis(2)]


while 1:
    for event in pygame.event.get():
        joystickValues = [-my_joystick.get_axis(0) - joystickoffsets[0],
                          -my_joystick.get_axis(1) - joystickoffsets[1],
                          my_joystick.get_axis(2) - joystickoffsets[2] - 0.160766601562,
                          -my_joystick.get_axis(3)]
        # ADD GET_BUTTON STATEMENT HERE

        for i in range(len(joystickValues)):
            if -0.000000001 < joystickValues[i] < 0.000000001:
                joystickValues[i] = 0
            elif joystickValues[i] > 1:
                joystickValues[i] = 1
            elif joystickValues[i] < -1:
                joystickValues[i] = -1
        print str(joystickValues[0]) + "\t" + str(joystickValues[1]) + "\t" + str(joystickValues[2]) + \
              '\t' + str(joystickValues[3])
        payload = {'x1': joystickValues[2], 'y1': joystickValues[3], 'x2': joystickValues[0],
                   'y2': joystickValues[1], 'btn': {}} #ADD BUTTON UPLOAD HERE
        response = requests.post(baseURL + controllerURL, data=payload)
        clock.tick(40)

pygame.quit ()
