// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

var joystick_values = {'x1' : 0, 'y1': 0, 'x2': 0, 'y2':0, 'btn': {} };
var target = {'mode': "none", 'alt': 0, 'long': 0, "lat": 0};
var dataPacket = {'speed': 0, 'altitude': 0, 'direction': 0};
var command = "none";

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

app.use(bodyParser.json())

app.get('/values', function(req, res) {
    res.json(joystick_values)
});

app.post('/values', function(req, res) {
    res.send("values received")
    joystick_values = req.body
    console.log(joystick_values)
});

app.post('/procedure', function(req, res) {
    res.send("values received")
    target = req.body
});

app.get('/procedure', function(req, res) {
    res.json(target)
});

app.post('/data', function(req, res) {
    res.send("values received")
    dataPacket = req.body
});

app.get("/data", function(req, res) {
    res.json(dataPacket)
});

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
